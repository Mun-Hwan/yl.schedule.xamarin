﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Schedule.Xamarin.Models
{
    public class ScItem
    {
        public Schedule Schedule { get; set; }
        public Member Member { get; set; }
    }
}
