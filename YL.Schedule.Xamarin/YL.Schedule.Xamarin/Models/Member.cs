﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Schedule.Xamarin.Models
{
    public class Member
    {
        public string Id { get; set; } = "CMH";
        public string Name { get; set; }
        public string HP { get; set; }
        public string Dept { get; set; } = "BATH";
        public bool UseBit { get; set; }
        public string InsertId { get; set; }
        public DateTime InsertDt { get; set; }
        public string UpdateId { get; set; }
        public DateTime UpdateDt { get; set; }
    }

    public class LoginUser
    {
        public string Id { get; set; }
        public string Pass { get; set; }
    }
}
