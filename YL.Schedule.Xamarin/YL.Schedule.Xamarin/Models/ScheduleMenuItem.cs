﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YL.Schedule.Xamarin.Models
{
    public enum ScheduleItemType
    {
        My,
        All
    }

    public class ScheduleMenuItem
    {
        public ScheduleItemType Id { get; set; }
        public string Title { get; set; }
    }
}
