﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using YL.Schedule.Xamarin.Models;
using YL.Schedule.Xamarin.ViewModels;

namespace YL.Schedule.Xamarin.Common
{
    public class SharedPreference : BaseViewModel
    {

        HttpClient m_client;

        #region single-ton

        private SharedPreference()
        {
            m_client = new HttpClient
            {
                MaxResponseContentBufferSize = 256000
            };

            SalesNameToId = new Dictionary<string, string>
            {
                { "미지정", "NOTHING" }, { "심현아", "SHA" },
                { "조재선", "JJS" }, { "박은희", "PEH" },
                { "조현나", "CHN" }, { "백인성", "BIS" },
                { "차문환", "CMH" }
            };

            SalesIdToName = new Dictionary<string, string>
            {
                { "NOTHING", "미지정" }, { "SHA", "심현아" },
                { "JJS", "조재선" }, { "PEH", "박은희" },
                { "CHN", "조현나" }, { "BIS", "백인성" },
                { "CMH", "차문환" }
            };

            VisitTimes = new List<string>
            {
                { "미지정" },
                { "08:00" }, { "09:00" }, { "10:00" },
                { "11:00" }, { "12:00" }, { "13:00" },
                { "14:00" }, { "15:00" }, { "16:00" },
                { "17:00" }, { "18:00" }, { "19:00" },
                { "20:00" }, { "오전중" }, { "오후중" }
            };

        }

        private static SharedPreference m_Instance;
        public static SharedPreference Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new SharedPreference();
                }
                return m_Instance;
            }
            set
            {
                m_Instance = value;
            }
        }
        
        #endregion


        #region property

        private static Member m_LoginUser;
        public Member LoginUser
        {
            get
            {
                if (m_LoginUser == null)
                {
                    m_LoginUser = new Member();
                }
                return m_LoginUser;
            }
            set
            {
                SetProperty(ref m_LoginUser, value);
            }
        }

        private static Dictionary<string, string> m_SalesNameToId;
        public Dictionary<string, string> SalesNameToId
        {
            get
            {
                return m_SalesNameToId;
            }
            set
            {
                m_SalesNameToId = value;
            }
        }

        private static Dictionary<string, string> m_SalesIdToName;
        public Dictionary<string, string> SalesIdToName
        {
            get
            {
                return m_SalesIdToName;
            }
            set
            {
                m_SalesIdToName = value;
            }
        }

        private static List<string> m_VisitTimes;
        public List<string> VisitTimes
        {
            get
            {
                return m_VisitTimes;
            }
            set
            {
                m_VisitTimes = value;
            }
        }

        public string m_CurrentPageName { get; set; }

        #endregion


        #region method

        /// <summary>
        /// 이미지 전송
        /// </summary>
        /// <param name="path"></param>
        async public Task<string> HttpSendImageAsync(string path)
        {
            var addrStr = $@"http://192.168.50.89/Member/LoginCheck";
            //var host = Config.Host;
            //var port = Config.Port;
            //var addrStr = new StringBuilder();
            //addrStr.Append($@"http://");
            //addrStr.Append(host);
            //addrStr.Append($@":");
            //addrStr.Append(port);
            //addrStr.Append($@"/Analysis/Leaf");

            var resultString = string.Empty;

            string x = await Task.Factory.StartNew(() =>
            {
                using (var stream = File.Open(path, FileMode.Open))
                {
                    var files = new[]
                    {
                        new HttpUploadFile
                        {
                            Name = "files",
                            Filename = Path.GetFileName(path),
                            ContentType = "text/plain",
                            Stream = stream
                        }
                    };

                    var values = new NameValueCollection
                    {
                        { "id", "VIP" },
                        { "pass", "John Doe" },
                    };

                    byte[] result = new HttpUploadFile().HttpUploadFiles(addrStr.ToString(), files, values);
                    resultString = Encoding.UTF8.GetString(result);

                    return resultString;
                }
            });

            return x;
        }

        /// <summary>
        /// 텍스트 전송
        /// </summary>
        /// <param name="path"></param>
        public async Task<object> HttpPostAsync(object item, string url)
        {
            //var uri = $@"http://192.168.50.89/Member/LoginCheck";
            var json = JsonConvert.SerializeObject(item);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await m_client.PostAsync(url, content);

            var returnString = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                returnString = await response.Content.ReadAsStringAsync();
            }
            //var returnJson = JsonConvert.DeserializeObject(returnString);

            return returnString;
        }


        /// <summary>
        /// 텍스트 전송
        /// </summary>
        /// <param name="path"></param>
        public async Task<object> HttpPutAsync(object item, string url)
        {
            var json = JsonConvert.SerializeObject(item);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await m_client.PutAsync(url, content);

            var returnString = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                returnString = await response.Content.ReadAsStringAsync();
            }
            //var returnJson = JsonConvert.DeserializeObject(returnString);

            return returnString;
        }

        /// <summary>
        /// 텍스트 전송
        /// </summary>
        /// <param name="path"></param>
        async public Task<object> HttpGetAsync(string item, string url)
        {
            //var uri = $@"http://192.168.50.89/Member/LoginCheck";
            //var json = JsonConvert.SerializeObject(item);

            //var param = string.Empty;
            //if (item != "")
            //{
            //    param += "/ " + item;
            //}

            HttpResponseMessage response = null;
            response = await m_client.GetAsync(url + "/" + item);

            var returnString = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                returnString = await response.Content.ReadAsStringAsync();
            }
            //var returnJson = JsonConvert.DeserializeObject(returnString);

            return returnString;
        }


        /// <summary>
        /// 텍스트 전송
        /// </summary>
        /// <param name="path"></param>
        async public Task<object> HttpGetAsync(UrlParams urlParams)
        {
            HttpResponseMessage response = null;
            response = await m_client.GetAsync(urlParams.Url + "/" + urlParams.Dept + "/" + urlParams.Id + "/" + urlParams.Date.ToString("yyyy-MM-dd"));

            var returnString = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                returnString = await response.Content.ReadAsStringAsync();
            }
            //var returnJson = JsonConvert.DeserializeObject(returnString);

            return returnString;
        }


        /// <summary>
        /// 텍스트 전송
        /// </summary>
        /// <param name="path"></param>
        async public Task<object> HttpGetAllAsync(UrlParams urlParams)
        {
            HttpResponseMessage response = null;
            response = await m_client.GetAsync(urlParams.Url + "/" + urlParams.Dept + "/" + urlParams.Date.ToString("yyyy-MM-dd"));

            var returnString = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                returnString = await response.Content.ReadAsStringAsync();
            }
            //var returnJson = JsonConvert.DeserializeObject(returnString);

            return returnString;
        }


        //public object HttpPost(object item, string url)
        //{
        //    if (url == null)
        //    {
        //        return new Dictionary<string, object>
        //        {
        //            { "result", "FAIL" },
        //            { "d", "" }
        //        };
        //    }
        //
        //    //var uri = $@"http://192.168.50.89/Member/LoginCheck";
        //    var json = JsonConvert.SerializeObject(item);
        //    var content = new StringContent(json, Encoding.UTF8, "application/json");
        //
        //    HttpResponseMessage response = null;
        //    response = m_client.PostAsync(url, content).Result;
        //
        //    var returnString = string.Empty;
        //
        //    if (response.IsSuccessStatusCode)
        //    {
        //        returnString = response.Content.ReadAsStringAsync().Result;
        //    }
        //    var returnJson = JsonConvert.DeserializeObject(returnString);
        //
        //    return returnJson;
        //}

        #endregion

    }

    public class UrlParams
    {
        public string Dept { get; set; }
        public string Id { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }
    }

}
