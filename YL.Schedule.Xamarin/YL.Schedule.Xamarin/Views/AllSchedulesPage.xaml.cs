﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YL.Schedule.Xamarin.Common;
using YL.Schedule.Xamarin.Models;
using YL.Schedule.Xamarin.ViewModels;

namespace YL.Schedule.Xamarin.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AllSchedulesPage : ContentPage
	{

        AllScheduleViewModel viewModel;

        public AllSchedulesPage ()
		{
            InitializeComponent();

            SharedPreference.Instance.m_CurrentPageName = "AllSchedulesPage";

            BindingContext = viewModel = new AllScheduleViewModel();

            //viewModel.IsBusy = false;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as ScItem;
            if (item == null)
                return;

            await Navigation.PushModalAsync(new NavigationPage(new ScheduleDetailPage(new ScheduleDetailViewModel(item))));

            // Manually deselect item.
            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewSchedulePage(viewModel.CurDate)));
        }

        //protected override void OnAppearing()
        //{
        //    base.OnAppearing();

        //    if (viewModel.Items.Count == 0)
        //        viewModel.LoadSchedulesCommand.Execute(null);
        //}

        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            viewModel.LoadSchedulesCommand.Execute(null);
        }

    }
}