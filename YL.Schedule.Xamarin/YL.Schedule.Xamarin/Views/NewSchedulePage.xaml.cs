﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YL.Schedule.Xamarin.Common;
using YL.Schedule.Xamarin.Models;
using YL.Schedule.Xamarin.ViewModels;

namespace YL.Schedule.Xamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewSchedulePage : ContentPage
    {
        public ScItem ScItem { get; set; }
        public DateTime CurDate { get; set; }

        public NewSchedulePage()
        {
            InitializeComponent();

            ScItem = new ScItem
            {
                Schedule = new Models.Schedule
                {
                    //Dept = SharedPreference.Instance.LoginUser.Dept,
                    Dept = "BATH",
                    VisitDate = DateTime.Now
                },
                Member = null
            };

            foreach (var salesName in SharedPreference.Instance.SalesNameToId.Keys)
            {
                ManagerId.Items.Add(salesName);
            };
            // 미지정 기본 선택
            ManagerId.SelectedIndex = 0;

            foreach (var item in SharedPreference.Instance.VisitTimes)
            {
                VisitTime.Items.Add(item);
            }
            // 미지정 기본 선택
            VisitTime.SelectedIndex = 0;

            BindingContext = this;
        }

        public NewSchedulePage(DateTime curDate) : this()
        {
            ScItem.Schedule.VisitDate = VisitDate.Date = curDate;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            var _guid = Guid.NewGuid().ToString();

            var TargetMsgCenter = string.Empty;
            switch (SharedPreference.Instance.m_CurrentPageName)
            {
                case "MySchedulesPage":
                    TargetMsgCenter = "My";
                    break;
                case "AllSchedulesPage":
                    TargetMsgCenter = "All";
                    break;
                default:
                    break;
            }

            if (swBath.IsToggled)
            {
                await Task.Run(() =>
                {
                    MessagingCenter.Send(this, TargetMsgCenter + "AddItem", new ScItem
                    {
                        Schedule = new Models.Schedule
                        {
                            Dept = "BATH",
                            ManagerId = (SharedPreference.Instance.LoginUser.Dept.Equals("BATH") ? SharedPreference.Instance.SalesNameToId[ManagerId.Items[ManagerId.SelectedIndex]] : "NOTHING"),
                            VisitDate = VisitDate.Date,
                            VisitTime = VisitTime.Items[VisitTime.SelectedIndex],
                            ReVisit = ReVisit.Text,
                            Agency = Agency.Text,
                            CusName = CusName.Text,
                            CusHP = CusHP.Text,
                            CusAddr = CusAddr.Text,
                            PredItems = PredItems.Text,
                            Remark = Remark.Text,
                            TokenIdx = _guid,
                            UseBit = true,
                            //InsertId = SharedPreference.Instance.LoginUser.Id,
                            InsertId = "CMH",
                            InsertDt = DateTime.Now,
                            //UpdateId = SharedPreference.Instance.LoginUser.Id,
                            UpdateId = "CMH",
                            UpdateDt = DateTime.Now
                        },
                        Member = null
                    });
                });
            }

            if (swKitchen.IsToggled)
            {
                await Task.Run(() =>
                {
                    MessagingCenter.Send(this, TargetMsgCenter + "AddItem", new ScItem
                    {
                        Schedule = new Models.Schedule
                        {
                            Dept = "KITCHEN",
                            ManagerId = (SharedPreference.Instance.LoginUser.Dept.Equals("KITCHEN") ? SharedPreference.Instance.SalesNameToId[ManagerId.Items[ManagerId.SelectedIndex]] : "NOTHING"),
                            VisitDate = VisitDate.Date,
                            VisitTime = VisitTime.Items[VisitTime.SelectedIndex],
                            ReVisit = ReVisit.Text,
                            Agency = Agency.Text,
                            CusName = CusName.Text,
                            CusHP = CusHP.Text,
                            CusAddr = CusAddr.Text,
                            PredItems = PredItems.Text,
                            Remark = Remark.Text,
                            TokenIdx = _guid,
                            UseBit = true,
                            //InsertId = SharedPreference.Instance.LoginUser.Id,
                            InsertId = "CMH",
                            InsertDt = DateTime.Now,
                            //UpdateId = SharedPreference.Instance.LoginUser.Id,
                            UpdateId = "CMH",
                            UpdateDt = DateTime.Now
                        },
                        Member = null
                    });
                });
            }

            await Navigation.PopModalAsync();

        }
    }
}