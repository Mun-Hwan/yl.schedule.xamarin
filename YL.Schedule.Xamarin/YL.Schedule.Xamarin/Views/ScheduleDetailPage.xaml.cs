﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YL.Schedule.Xamarin.Common;
using YL.Schedule.Xamarin.Models;
using YL.Schedule.Xamarin.ViewModels;

namespace YL.Schedule.Xamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScheduleDetailPage : ContentPage
    {
        ScheduleDetailViewModel viewModel;

        public ScheduleDetailPage(ScheduleDetailViewModel viewModel)
        {
            InitializeComponent();
            foreach (var salesName in SharedPreference.Instance.SalesNameToId.Keys)
            {
                ManagerId.Items.Add(salesName);
            };
            // 영업담당자 선택
            var items = SharedPreference.Instance.SalesNameToId.FirstOrDefault(x => x.Value.Equals(viewModel.ScItem.Schedule.ManagerId));
            ManagerId.SelectedIndex = ManagerId.Items.IndexOf(items.Key);

            foreach (var item in SharedPreference.Instance.VisitTimes)
            {
                VisitTime.Items.Add(item);
            }
            // 방문시간 선택
            VisitTime.SelectedIndex = VisitTime.Items.IndexOf(viewModel.ScItem.Schedule.VisitTime); ;


            BindingContext = this.viewModel = viewModel;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {

            var TargetMsgCenter = string.Empty;
            switch (SharedPreference.Instance.m_CurrentPageName)
            {
                case "MySchedulesPage":
                    TargetMsgCenter = "My";
                    break;
                case "AllSchedulesPage":
                    TargetMsgCenter = "All";
                    break;
                default:
                    break;
            }

            await Task.Run(() =>
            {
                MessagingCenter.Send(this, TargetMsgCenter + "UpdateItem", new ScItem
                {
                    Schedule = new Models.Schedule
                    {
                        Idx = viewModel.ScItem.Schedule.Idx,
                        Dept = Dept.Text,
                        ManagerId = SharedPreference.Instance.SalesNameToId[ManagerId.Items[ManagerId.SelectedIndex]],
                        VisitDate = VisitDate.Date,
                        //VisitDate = VisitDate.Text,
                        VisitTime = VisitTime.Items[VisitTime.SelectedIndex],
                        ReVisit = ReVisit.Text,
                        Agency = Agency.Text,
                        CusName = CusName.Text,
                        CusHP = CusHP.Text,
                        CusAddr = CusAddr.Text,
                        PredItems = PredItems.Text,
                        Remark = Remark.Text,
                        TokenIdx = Guid.NewGuid().ToString(),
                        UseBit = true,
                        //InsertId = SharedPreference.Instance.LoginUser.Id,
                        InsertId = "CMH",
                        InsertDt = DateTime.Now,
                        //UpdateId = SharedPreference.Instance.LoginUser.Id,
                        UpdateId = "CMH",
                        UpdateDt = DateTime.Now
                    },
                    Member = null
                });
            });

            await Navigation.PopModalAsync();
        }

    }
}