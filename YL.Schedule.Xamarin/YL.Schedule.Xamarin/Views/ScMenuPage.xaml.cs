﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YL.Schedule.Xamarin.Models;

namespace YL.Schedule.Xamarin.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScMenuPage : ContentPage
	{
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<ScheduleMenuItem> menuItems;

        public ScMenuPage ()
		{
			InitializeComponent ();

            menuItems = new List<ScheduleMenuItem>
            {
                new ScheduleMenuItem {Id = ScheduleItemType.My, Title="내 일정" },
                new ScheduleMenuItem {Id = ScheduleItemType.All, Title="전체 일정" }
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((ScheduleMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };

        }
    }
}