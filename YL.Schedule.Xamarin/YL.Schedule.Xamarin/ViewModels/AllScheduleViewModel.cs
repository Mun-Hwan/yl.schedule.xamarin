﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using YL.Schedule.Xamarin.Common;
using YL.Schedule.Xamarin.Models;
using YL.Schedule.Xamarin.Views;

namespace YL.Schedule.Xamarin.ViewModels
{
    public class AllScheduleViewModel : BaseViewModel
    {
        ObservableCollection<ScItem> items;
        public ObservableCollection<ScItem> Items
        {
            get { return items; }
            set { SetProperty(ref items, value); }
        }

        DateTime curDate;
        public DateTime CurDate
        {
            get { return curDate; }
            set { SetProperty(ref curDate, value); }
        }

        string managerId;
        public string ManagerId
        {
            get { return managerId; }
            set { SetProperty(ref managerId, value); }
        }

        public Command LoadSchedulesCommand { get; set; }
        public Command PrevDateCommand { get; set; }
        public Command NextDateCommand { get; set; }


        public AllScheduleViewModel()
        {
            CurDate = DateTime.Today;
            Title = "전체 일정";
            Items = new ObservableCollection<ScItem>();
            LoadSchedulesCommand = new Command(async () => await ExecuteLoadSchedulesCommand());
            PrevDateCommand = new Command(async () => await ExecutePrevDateCommand());
            NextDateCommand = new Command(async () => await ExecuteNextDateCommand());
            var tasks = new List<Task>();

            MessagingCenter.Subscribe<NewSchedulePage, ScItem>(this, "AllAddItem", (obj, item) =>
            {
                var _item = item as ScItem;
                var x = ExecuteAddSchedulesCommand(_item);
                tasks.Add(x);
                Task.WaitAll(tasks.ToArray());
            });

            MessagingCenter.Subscribe<ScheduleDetailPage, ScItem>(this, "AllUpdateItem", (obj, item) =>
            {
                var _item = item as ScItem;
                var x = ExecuteUpdateSchedulesCommand(_item);
                tasks.Add(x);
                Task.WaitAll(tasks.ToArray());
            });

        }

        async Task ExecuteLoadSchedulesCommand()
        {
            await Task.Run(async () =>
           {
               if (IsBusy)
                   return;

               IsBusy = true;

               try
               {
                   Items.Clear();

                   var param = new UrlParams
                   {
                       Dept = SharedPreference.Instance.LoginUser.Dept,
                       Id = SharedPreference.Instance.LoginUser.Id,
                       Url = $@"http://192.168.50.89/Schedule/GetAll",
                       Date = CurDate
                   };

                   var returnObj = await SharedPreference.Instance.HttpGetAllAsync(param);
                   var json = JObject.Parse(returnObj.ToString());
                   if ((string)json["result"] == "OK")
                   {
                       foreach (var item in json["data"])
                       {
                           var jStr = JsonConvert.SerializeObject(item);
                           var i = JsonConvert.DeserializeObject<ScItem>(jStr);
                           if (i.Member == null)
                           {
                               var _m = new Member
                               {
                                   Id = i.Schedule.ManagerId,
                                   Name = SharedPreference.Instance.SalesIdToName[i.Schedule.ManagerId]
                               };

                               i.Member = _m;
                           }
                           else
                           {
                               i.Member.Id = i.Schedule.ManagerId;
                               i.Member.Name = SharedPreference.Instance.SalesIdToName[i.Schedule.ManagerId];
                           }

                           Items.Add(i);
                       }
                   }
                   else
                   {
                       this.IsBusy = false;
                   }
               }
               catch (Exception ex)
               {
                   Debug.WriteLine(ex);
               }
               finally
               {
                   IsBusy = false;
               }
           });
        }

        async Task ExecuteAddSchedulesCommand(ScItem item)
        {
            await Task.Run(async () =>
            {
                if (IsBusy)
                    return;

                IsBusy = true;

                try
                {
                    var url = $@"http://192.168.50.89/Schedule/Add";
                    var returnObj = await SharedPreference.Instance.HttpPostAsync(item, url);
                    var json = JObject.Parse(returnObj.ToString());
                    if ((string)json["result"] == "OK")
                    {
                        if (item.Schedule.VisitDate.ToString("yyyy-MM-dd").Equals(CurDate.ToString("yyyy-MM-dd"))
                            && item.Schedule.Dept.Equals(SharedPreference.Instance.LoginUser.Dept))
                        {
                            Items.Add(item);
                        }
                    }
                    else
                    {
                        this.IsBusy = false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    IsBusy = false;
                }
            });
        }

        async Task ExecuteUpdateSchedulesCommand(ScItem item)
        {
            await Task.Run(async () =>
            {
                if (IsBusy)
                    return;

                IsBusy = true;

                try
                {
                    var url = $@"http://192.168.50.89/Schedule/Put";
                    var returnObj = await SharedPreference.Instance.HttpPutAsync(item, url);
                    var json = JObject.Parse(returnObj.ToString());
                    if ((string)json["result"] == "OK")
                    {
                        var scItem = Items.FirstOrDefault(o => o.Schedule.Idx == item.Schedule.Idx);
                        Items.Remove(scItem);

                        if (item.Schedule.VisitDate.ToString("yyyy-MM-dd").Equals(CurDate.ToString("yyyy-MM-dd")))
                        {
                            Items.Add(item);
                        }
                    }
                    else
                    {
                        this.IsBusy = false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    IsBusy = false;
                }
            });
        }

        async Task ExecutePrevDateCommand()
        {
            await Task.Run(() =>
            {
                CurDate = CurDate.AddDays(-1);
            });
            //await Task.Run(() => ExecuteLoadSchedulesCommand());
        }

        async Task ExecuteNextDateCommand()
        {
            await Task.Run(() =>
            {
                CurDate = CurDate.AddDays(1);
            });
            //await Task.Run(() => ExecuteLoadSchedulesCommand());
        }

    }

}
