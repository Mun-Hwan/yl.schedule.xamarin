﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using YL.Schedule.Xamarin.Common;
using YL.Schedule.Xamarin.Models;
using YL.Schedule.Xamarin.Views;

namespace YL.Schedule.Xamarin.ViewModels
{
    public class ScheduleDetailViewModel : BaseViewModel
    {
        public ScItem ScItem { get; set; }

        public ScheduleDetailViewModel(ScItem scItem = null)
        {
            ScItem = scItem;

            //MessagingCenter.Subscribe<ScheduleDetailPage, ScItem>(this, "UpdateItem", async (obj, item) =>
            //{
            //    var _item = item as ScItem;
            //    await ExecuteUpdateSchedulesCommand(_item);
            //});
        }

        //async Task ExecuteUpdateSchedulesCommand(ScItem item)
        //{
        //    if (IsBusy)
        //        return;

        //    IsBusy = true;

        //    try
        //    {
        //        var url = $@"http://192.168.50.89/Schedule/Put";
        //        var returnObj = await SharedPreference.Instance.HttpPutAsync(item, url);
        //        var json = JObject.Parse(returnObj.ToString());
        //        if ((string)json["result"] == "OK")
        //        {
        //            var jStr = JsonConvert.SerializeObject(json["data"]);
        //            var i = JsonConvert.DeserializeObject<ScItem>(jStr);
        //            ScItem = i;
        //        }
        //        else
        //        {
        //            this.IsBusy = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex);
        //    }
        //    finally
        //    {
        //        IsBusy = false;
        //    }
        //}

    }
}
